var hello = require('../hello');
describe('Hello', function(){
    it('Tem que aparecer "Hello world!"', function(){
        var text = hello();
        expect(text).toEqual('Hello world!');
    })
})