var isAnagram = require('../anagram');
describe('Anagram', function(){
    it('is true when "abc" and "cba"', function(){
        expect(isAnagram('abc', 'cba')).toEqual(true);
    })
    it('is true when "Amor" and "Roma"', function(){
        expect(isAnagram('Amor', 'Roma')).toEqual(true);
    })
    it('is true when "123" and "321"', function(){
        expect(isAnagram('123', '321')).toEqual(true);
    })
})